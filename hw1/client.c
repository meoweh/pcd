#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <time.h>

#define MESSAGE_MAX_SIZE 65535
#define PORT 2026


int main (int argc, char *argv[]) {
	int sd;
	struct sockaddr_in server;	
	int pkgs_sent = 0;
	long long bytes_sent = 0;
	char filename[100], msg[MESSAGE_MAX_SIZE+2];

	if(argc < 5){
		perror("[client] Numar insuficient de argumente.\n");
		return errno;
	}

	strcpy(filename, argv[4]);
	int file_in = open(filename, O_RDONLY);

	int port = PORT;
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr("192.168.0.41");
	server.sin_port = htons (port);

	int buffer_size = atoi(argv[3]);
	struct pollfd poll_fd;

	if(!strcmp(argv[1], "tcp")) {
		if ((sd = socket (AF_INET, SOCK_STREAM, 0)) == -1) {
			perror ("Eroare la socket().\n");
			return errno;
		}
		
		if (connect (sd, (struct sockaddr *) &server,sizeof (struct sockaddr)) == -1) {
			perror ("[client]Eroare la connect().\n");
			return errno;
		}
		poll_fd.fd = sd;
		poll_fd.events = POLLIN;
		clock_t begin = clock();
		while(1) {
			bzero (msg, buffer_size);
			if(read (file_in, msg, buffer_size) <= 0) {
				printf("[client] Nu mai avem ce citi.\n");
				break;
			}

			if (write (sd, msg, buffer_size) <= 0) {
				perror ("[client]Eroare la write() spre server.\n");
				return errno;
			}
			printf("[client] Am trimis un pachet\n");

			bytes_sent += buffer_size;
			pkgs_sent++;

			if(!strcmp(argv[2], "wait")) {
				int poll_ret = poll(&poll_fd, 1, 1000);
				if(poll_ret == 0) {
					//timeout, resend
					printf("[client] Pierdut pachet... retrimitem\n");
					if (write (sd, msg, buffer_size) <= 0) {
			    		perror ("[client]Eroare la sendto() spre server.\n");
			    		return errno;
					}
				} else {
					read(sd, msg,3);
					printf("[client] Am primit un pachet ack\n");	
				}
				
			}
		}
		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		printf("Time spent: %f\n", time_spent);
		printf("Bytes sent: %lld\n", bytes_sent);
		printf("Pkgs sent: %d\n", pkgs_sent);
		close (sd);
		return 0;
	} 
	if(!strcmp(argv[1], "udp")){ //udp
		if ((sd = socket (AF_INET, SOCK_DGRAM, 0)) == -1) {
	    	perror ("Eroare la socket().\n");
	    	return errno;
    	}
    	poll_fd.fd = sd;
		poll_fd.events = POLLIN;
    	int length = sizeof(server);
    	int msglen;
    	clock_t begin = clock();
    	while(1) {
	    	bzero (msg, MESSAGE_MAX_SIZE);
			if(read (file_in, msg, buffer_size) <= 0) {
				printf("[client] Nu mai avem ce citi.\n");
				break;
			}
			if (sendto (sd, msg, buffer_size, 0, (struct sockaddr*)&server, length) <= 0) {
			    perror ("[client] Eroare la sendto() spre server.\n");
			    return errno;
			}
			printf("[client] Am trimis un pachet\n");
			bytes_sent += buffer_size;
			pkgs_sent++;
			if(!strcmp(argv[2], "wait")) {
				int poll_ret = poll(&poll_fd, 1, 1000);
				if(poll_ret == 0) {
					//timeout, resend
					printf("[client] Pierdut pachet... retrimitem\n");
					if (sendto (sd, msg, buffer_size,0, (struct sockaddr*)&server, length) <= 0) {
			    		perror ("[client]Eroare la sendto() spre server.\n");
			    		return errno;
					}
				} else {
					recvfrom(sd, msg, 3, 0,(struct sockaddr*)&server, &length);
					printf("[client] Am primit un pachet ack\n");
				}
			}
		}
		sendto (sd, "", 0, 0, (struct sockaddr*)&server, length);
		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		printf("Time spent: %f\n", time_spent);
		printf("Bytes sent: %lld\n", bytes_sent);
		printf("Pkgs sent: %d\n", pkgs_sent);
		close (sd);
		return 0;
	} 
	
}
